#include <stdio.h>

int main() {
    int number;

    printf("Favor inserir um número: ");
    scanf("%d", &number);

    if ((number >= 100) && (number <= 200)) {
        printf("Você digitou um número entre 100 e 200.\n");
    } else {
        printf("Você digitou um número fora da faixa entre 100 e 200.\n");
    }

    return 0;
}

