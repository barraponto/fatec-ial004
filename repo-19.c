#include <stdio.h>
#include <string.h>
#include "util.h"

const int VECTOR_SIZE = 3;
const int NAME_SIZE = 128;

int main() {
    char names[VECTOR_SIZE][NAME_SIZE];

    for (int i = 0; i < VECTOR_SIZE; i++) {
        printf("Por favor, insira um nome: ");
        fgets(names[i], NAME_SIZE, stdin);
        trim_newlines(names[i]);
    }

    printf("\n");

    for (int i = 0; i < VECTOR_SIZE; i++) {
        int count_a = 0;
        int count_e = 0;

        for (int j = 0; j < strlen(names[i]); j++) {
            if ((names[i][j] == 'a') || (names[i][j] == 'A')) { count_a++; }
            if ((names[i][j] == 'e') || (names[i][j] == 'E')) { count_e++; }
        }

        printf("O nome '%s' tem %d letras 'A' e %d letras 'E'.\n", names[i], count_a, count_e);
    }

    return 0;
}

