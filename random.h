#include <stdlib.h>
#include <time.h>

void initializePRNG() {
    srand(time(NULL));
}

int random_integer(int minimum, int maximum) {
    return minimum + (rand() % (maximum - minimum));
}
