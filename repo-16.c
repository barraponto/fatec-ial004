#include <stdio.h>

int main() {
    int age;
    int stop = 0;
    int above50 = 0;
    int below21 = 0;

    while (!stop) {
        printf("Insira a idade de uma pessoa (-99 para terminar): ");
        scanf("%d", &age);
        if (age == -99) { stop = 1; }
        else if (age < 0) { printf("Idade inválida.\n"); }
        else if (age > 50) { above50++; }
        else if (age < 21) { below21++; }
    }

    printf("Maiores de 50 anos: %d\n", above50);
    printf("Menores de 21 anos: %d\n", below21);

    return 0;
}

