#include <stdio.h>
#include "random.h"

const int MIN_NUMBER = 1;
const int MAX_NUMBER = 32;
const int VECTOR_SIZE = 5;

void print_ints(int vector[], int size) {
    printf("[");
    for (int i = 0; i < size; i++) {
        printf("%d", vector[i]);
        if (i < size - 1) { printf(", "); }
    }
    printf("]");
}

int main() {
    initializePRNG();

    int vector_A[VECTOR_SIZE];
    int vector_B[VECTOR_SIZE];

    for (int i = 0; i < VECTOR_SIZE; i++) {
        vector_A[i] = random_integer(MIN_NUMBER, MAX_NUMBER);
        vector_B[i] = random_integer(MIN_NUMBER, MAX_NUMBER);
    }

    printf("Vetor A: ");
    print_ints(vector_A, VECTOR_SIZE);
    printf("\n");

    printf("Vetor B: ");
    print_ints(vector_B, VECTOR_SIZE);
    printf("\n");

    printf("\n");
    for (int i = 0; i < VECTOR_SIZE; i++) {
        printf("A soma dos números na posição #%d é: %d\n", i, vector_A[i] + vector_B[i]);
    }

    return 0;
}

