#include <stdio.h>
#include "random.h"

const int NUMBERS_LENGTH = 8;
const int MIN_NUMBER = 1;
const int MAX_NUMBER = 32;

int main() {
    int candidate;
    int numbers[NUMBERS_LENGTH];
    int found = 0;

    initializePRNG();

    // populate an array with random integers.
    for (int i = 0; i < NUMBERS_LENGTH; i++) {
        numbers[i] = random_integer(MIN_NUMBER, MAX_NUMBER);
    }

    printf("Insira um número entre %d e %d: ", MIN_NUMBER, MAX_NUMBER);
    scanf("%d", &candidate);

    for (int i = 0; i < NUMBERS_LENGTH; i++) {
        if (numbers[i] == candidate) {
            found = 1;
            printf("Número '%d' encontrado na posição #%d.\n", candidate, i);
        }
    }

    if (!found) {
        printf("Número '%d' não foi encontrado no vetor.\n", candidate);
    }

    return 0;
}

